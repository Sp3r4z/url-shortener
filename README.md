# Tiny little small URL shortener

A very simple url shortener, witch Apache .htaccess and which works on shared hosting.

## Inspired by:
- [url-shortener](https://github.com/bbrothers/url-shortener/) → general idea/concept
- [OwnShortener](https://github.com/treyssatvincent/OwnShortener) → administration page design

## Helped by:
- [KNACSS](https://github.com/alsacreations/KNACSS) : CSS stylesheet

## Installation:
- Get HTTPS enable on your hosting
- Put all `build/` files on your hosting root folder
- Go on your hosting into webroot folder
- In `admin/conf.php` change `ADM_PWD` on `l.4`
- In `admin/conf.php` change `PATH` on `l.5` according to your installation PATH
- Go to `PATH/admin`
- Enter your password
- Manage your links
- Save your links
- Enjoy!

## Known issues
- all `/`, in slangs are deny
