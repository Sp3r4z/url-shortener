<?php

require_once('admin/conf.php');

$links=json_decode(file_get_contents(LNK_URL), true);
$slang = preg_replace('/'.addcslashes(PATH,'/').'/', '', $_SERVER['REQUEST_URI']);

if(array_key_exists($slang, $links)){
	header('Location: ' . $links[$slang]);
}
else {
	if ($slang!='')
	header('HTTP/1.0 404 Not Found');

	printHeader();

	?>
	<body>
		<section class='container'>
			<?php if ($slang=='') { ?>
				<h1 class='text-center'>What's going on here?</h1>
				<p>I am just a poor, simple and tiny URL shortener, based on URL list into a boring JSON file!</p>
			<?php }
			else { ?>

				<h1 class='text-center'>What are you doing here?</h1>
				<p>There is no link matching your request: <strong><?=$slang?></strong> <br>Try again!</p>
			<?php } ?>

			<?php printFooter(); ?>
		</section>
	</body>
	</html>
	<?php
}
?>