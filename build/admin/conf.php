<?php
session_start();

define("ADM_PWD", "password");
define("PATH", "/");
define("LNK_URL", 'links.json');

if(isset($_POST['logout'])) $_SESSION['logMeShort'] = false;
if(isset($_POST['login']) && $_POST['password']===ADM_PWD) $_SESSION['logMeShort']=true;

function printHeader($admin=0) {
	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>TLSUS<?=($admin)?' - Administration page':''?></title>
		<link rel="stylesheet" href="<?=($admin)?'../':''?>assets/css/style.css">
	</head>
	<?php
}

function printFooter() {
	?>
	<footer>
		TLSUS (Tiny Little Small URL Shortener) — Sp3r4z – <a href="https://framagit.org/Sp3r4z/url-shortener">Project repository</a>
	</footer>
	<?php
}
