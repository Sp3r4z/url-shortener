<?php
require_once('conf.php');

printHeader(1);

?>

<body>

	<section class='container'>

		<?php
		$scheme=(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] !== 'on')?false:true;

		if(!$scheme) {
			?>
			<div class="alert--danger" role="alert">
				Your server doesn't use <strong>HTTPS</strong>, so your personnal informations could be intercepted!
			</div>
			<?php
		}

		if(isset($_SESSION['logMeShort']) && $_SESSION['logMeShort']) {
			if(isset($_POST['submit'])) {
				$table=[];
				$tmp = $_POST;

				if(count($tmp)) {
					foreach ($tmp['links'] as $key => $value) {
						if($value['sl']!=null && $value['rd']!=null)
						$table[$value['sl']]=$value['rd'];
					}
				}

				file_put_contents('../'.LNK_URL,json_encode($table));
			}

			$links = json_decode(file_get_contents('../'.LNK_URL), true);

			?>
			<h1 class="txtcenter">Welcome aboard captain!</h1>
			<h2>Manage your shorten URLs</h2>

			<form action="." method="post">
				<table>
					<tbody id='allLinks'>
						<?php
						$scheme=($scheme)?'https://':'http://';
						$base_url =$scheme.$_SERVER['SERVER_NAME'].'/'.trim($_SERVER['REQUEST_URI'],'/admin').'/';
						$index=0;
						if(count($links)) {
							foreach ($links as $key => $val) {
								?>
								<tr class="grid record">
									<td class='col-5'>
										<input type='text' placeholder='slang' name='links[<?=$index?>][sl]' value='<?=$key?>'>
									</td>
									<td class="txtcenter">→</td>
									<td class='col-6'><input type='text' placeholder='URL redirection' class='fullwidth' name='links[<?=$index?>][rd]' value='<?=$val?>'></td>
									<td class="txtcenter"><button type='button' class='deleteRecord  btn--danger'>X</button></td>
								</tr>
								<?php
								$index++;
							}
						}
						else {
							?>

							<tr class="grid-12">
								<td class='col-6'>
									<div class='input-group'>
										<div class='input-group-prepend'>
											<span class='input-group-text'><?=$base_url?></span>
										</div>
										<input type='text' placeholder='slang' name='links[0][sl]'>
									</div>
								</td>
								<td class='col-6'><input type='text' placeholder='URL redirection' class='fullwidth' name='links[0][rd]'></td>
								<td><button type='button' class='btn btn-danger'>X</button></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>

				<div class="generalButtons">
					<button id='addLink' type="button" class='btn--primary'>Add a link</button>
					<button type="submit" name="submit" class='btn--success'>Save all links</button>

					<button type="submit" name="logout" class='btn--inverse'>LOGOUT</button>
					<input class="fullwidth" type="password" hidden name="password" placeholder="Put your password here!">
				</div>
			</form>

			<?php
		}
		else {
			?>
			<h1 class="text-center">Please login!</h1>

			<form action="." method="post">
				<input class="fullwidth" type="password" name="password" placeholder="Put your password here!">
				<div class="generalButtons">
					<button type="submit" name="login" class='btn--success'>Sign in</button>
				</div>
			</form>
			<?php
		}

		printFooter();
		?>

	</section>

	<script type="text/javascript" src="../assets/js/main.js"></script>

</body>
</html>
