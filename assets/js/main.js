'use strict';

document.addEventListener('DOMContentLoaded', function() {
	const table=document.getElementById('allLinks');
	let nbLinks=0;
	document.getElementById('addLink').addEventListener('click',_=>{
		let tmp = table.lastElementChild,
		clone=tmp.cloneNode(true);

		clone.childNodes[1].childNodes[1].childNodes[3].value='';
		clone.childNodes[1].childNodes[1].childNodes[3].name=`links[${nbLinks}][sl]`;
		clone.childNodes[3].childNodes[0].value='';
		clone.childNodes[3].childNodes[0].name=`links[${nbLinks}][rd]`;
		nbLinks++;

		table.appendChild(clone);
	});

	table.addEventListener('click',c=>{
		const elm = c.target;
		if(elm.type === "button") {
			removeLink(elm);
		}
	})

	function removeLink(e) {
		(e=e.parentNode.parentNode).parentElement.removeChild(e);
	}

});
